﻿# -*- coding: utf-8 -*-
# Copyright 2018 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Borderless Expense',
    'version': '10.0.1.0.0',
    'summary': 'Borderless Modules',
    'author': 'Terrence Nzaywa for ACS',
    'website': 'http://absolutecorporatesolutions.nl',
    'category': 'Enterprise Specific Management',
    'sequence': 0,
    'depends': [
        'hr_expense'
    ],
    'demo': [],
    'data': [
        'views/expense.xml'
    ],
    'installable': True
}
