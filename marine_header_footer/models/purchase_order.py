# -*- coding: utf-8 -*-

from odoo import models, fields, api


class MarinePurchaseOrderLine(models.Model):
    _inherit = 'purchase.order'

    attn = fields.Many2one('res.partner', string='ATTN')
    authorized_by = fields.Many2one('res.users', string='Authorized by')
    authorized_date = fields.Date(string='Authorized Date')

class MarinePurchaseOrder(models.Model):
    _inherit = 'purchase.order.line'

    @api.onchange('product_qty', 'product_uom')
    def _onchange_quantity(self):
        ret = super(MarinePurchaseOrder, self)._onchange_quantity()
        if self.product_id:
            self.price_unit = self.product_id.standard_price
        return ret