# -*- coding: utf-8 -*-

from odoo import models, fields, api


class AccountInvoiceUnplannedCosts(models.Model):
    _inherit = 'account.invoice'

    unplanned_costs = fields.Float(string="Unplanned Costs")
    store = fields.Char(string="Store")
    authorized_by = fields.Many2one('res.users', string='Authorized by')
    authorized_date = fields.Date(string='Authorized Date')

    @api.one
    @api.depends('invoice_line_ids.price_subtotal', 'tax_line_ids.amount',
                 'currency_id', 'company_id', 'date_invoice', 'type', 'unplanned_costs')
    def _compute_amount(self):
        ret = super(AccountInvoiceUnplannedCosts, self)._compute_amount()
        self.amount_total = self.amount_untaxed + self.amount_tax + self.unplanned_costs
        return ret