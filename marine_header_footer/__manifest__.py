﻿# -*- coding: utf-8 -*-
# Copyright 2018 Sunflower IT (http://sunflowerweb.nl)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'Marine Header Footer',
    'version': '10.0.1.0.0',
    'summary': 'Marine Extra Module',
    'author': 'ACS',
    'website': 'http://absolutecorporatesolutions.com',
    'category': 'Enterprise Specific Management',
    'sequence': 0,
    'depends': [
        'report',
        'account',
        'purchase'
    ],
    'demo': [],
    'data': [
        'views/header_footer.xml',
        'views/account_invoice.xml',
        'views/purchase_order.xml',
        'views/report_invoice.xml',
        'views/report_purchaseorder.xml',
        'views/renaming.xml',
        'views/report_delivery.xml'
    ],
    'installable': True
}
